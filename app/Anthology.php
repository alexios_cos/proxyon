<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Anthology extends Model
{
    public $timestamps = false;

    public function compositions()
    {
        return $this->belongsToMany('App\Composition', 'covers');
    }

}
