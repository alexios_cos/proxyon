<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    public $timestamps = false;

    public function compositions()
    {
        return $this->belongsToMany('App\Composition', 'authors');
    }

}
