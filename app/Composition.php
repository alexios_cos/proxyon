<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Composition extends Model
{
    public const CREATED_AT = 'addition_date';

    public const UPDATED_AT = 'change_date';

    public function anthologies()
    {
        return $this->belongsToMany('App\Anthology', 'covers');
    }

    public function artists()
    {
        return $this->belongsToMany('App\Artist', 'authors');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'stickers');
    }

    public function instruments()
    {
        return $this->belongsToMany('App\Instrument', 'music_parts');
    }

    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }

    public function subGenre()
    {
        return $this->belongsTo('App\SubGenre');
    }

}
