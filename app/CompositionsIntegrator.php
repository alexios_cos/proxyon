<?php


namespace App;


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CompositionsIntegrator
{
    public $directory = 'compositions/';

    public function integrateCompositions($filesNames, $filesExtensions)
    {
        $newFilesNames = null;

        $filesAttributes = $this->getCompositionsAttributes($filesNames);

        foreach ($filesAttributes as $fileAttributes) {
            $newFilesNames[] = $fileAttributes['composition']['name'];
        }

        $filesPaths = $this->storeCompositions($filesNames, $newFilesNames, $filesExtensions);
        $index = 0;

        foreach ($filesAttributes as &$fileAttributes) {
            $fileAttributes['composition']['path'] = $filesPaths[$index];
            $fileAttributes['composition']['extension'] = $filesExtensions[$index];
            ++$index;
        }

        $this->insertCompositionsInfo($filesAttributes);
        return $filesAttributes;
    }

    public function getCompositionsAttributes($filesNames)
    {
        $filesAttributes = null;

        if (is_array($filesNames)) {
            foreach ($filesNames as $attributesIndex => $fileName) {
                $explodedFileName = explode('|', $fileName);

                foreach ($explodedFileName as $fileAttributes) {
                    $explodedFileAttributes = explode(':', $fileAttributes);

                    if ($explodedFileAttributes[0] == 'tags' || $explodedFileAttributes[0] == 'instruments') {
                        $attributeValues = explode(',', $explodedFileAttributes[1]);

                        foreach ($attributeValues as $valueIndex => $attributeValue) {
                            $filesAttributes[$attributesIndex][$explodedFileAttributes[0]][$valueIndex]['name'] = $attributeValue;
                        }

                    } else {
                        $attributeValues = explode(';', $explodedFileAttributes[1]);

                        foreach ($attributeValues as $attributeValue) {
                             $explodedAttributeValue = explode('=', $attributeValue);
                             $filesAttributes[$attributesIndex][$explodedFileAttributes[0]][$explodedAttributeValue[0]] = $explodedAttributeValue[1];
                        }

                    }

                }

            }

        } else {
            $explodedFileName = explode('|', $filesNames);

            foreach ($explodedFileName as $fileAttributes) {
                $explodedFileAttributes = explode(':', $fileAttributes);

                if ($explodedFileAttributes[0] == 'tags' || $explodedFileAttributes[0] == 'instruments') {
                    $attributeValues = explode(',', $explodedFileAttributes[1]);

                    foreach ($attributeValues as $valueIndex => $attributeValue) {
                        $filesAttributes[0][$explodedFileAttributes[0]][$valueIndex]['name'] = $attributeValue;
                    }

                } else {
                    $attributeValues = explode(';', $explodedFileAttributes[1]);

                    foreach ($attributeValues as $attributeValue) {
                        $explodedAttributeValue = explode('=', $attributeValue);
                        $filesAttributes[0][$explodedFileAttributes[0]][$explodedAttributeValue[0]] = $explodedAttributeValue[1];
                    }

                }

            }

        }

        return $filesAttributes;
    }

    public function storeCompositions($filesNames, $newFilesNames, $filesExtensions)
    {
        $filesPaths = [];
        $outerIndex = 0;

        foreach ($newFilesNames as &$newFileName) {
            $fileDirectories = null;
            $randomName = Str::random(20);
            $newFileName = $randomName;

            for ($innerIndex = 0; $innerIndex < 6; $innerIndex++) {
                $fileDirectories .= substr($newFileName, $innerIndex, 1) . '/';
            }

            $filePath = $this->directory . $fileDirectories . $newFileName . '.' . $filesExtensions[$outerIndex];
            Storage::disk('local')->move($this->directory . $filesNames[$outerIndex] . '.' . $filesExtensions[$outerIndex], $filePath);
            array_push($filesPaths, $filePath);
            ++$outerIndex;
        }

        return $filesPaths;
    }

    public function insertCompositionsInfo($filesAttributes)
    {
        $previousAnthology = null;
        $previousArtist = null;

        foreach ($filesAttributes as $fileAttributes) {
            $genre = Genre::firstOrCreate($fileAttributes['genre']);
            $subGenre = $genre->subGenres()->firstOrCreate($fileAttributes['subgenre']);

            $composition = new Composition();
            $composition->name = $fileAttributes['composition']['name'];
            $composition->extension = $fileAttributes['composition']['extension'];
            $composition->path = $fileAttributes['composition']['path'];
            $composition->price = (int) $fileAttributes['composition']['price'];
            $composition->sales = 0;
            $composition->marks = 0;

            if (isset($fileAttributes['composition']['license'])) {
                $composition->license = $fileAttributes['composition']['license'];
            }

            $composition->genre()->associate($genre);
            $composition->subGenre()->associate($subGenre);
            $composition->save();

            if (isset($fileAttributes['anthology'])) {
                if (isset($fileAttributes['anthology']['id'])) {
                    $anthology = Anthology::where('id', $fileAttributes['anthology']['id'])->first();
                    $composition->anthologies()->attach($anthology->id);
                } else {
                    if ($previousAnthology != $fileAttributes['anthology']['name']) {
                        $anthology = new Anthology();
                        $anthology->name = $fileAttributes['anthology']['name'];
                        $anthology->save();
                        $composition->anthologies()->attach($anthology->id);
                    } else {
                        $anthology = Anthology::where('name', $fileAttributes['anthology']['name'])->first();
                        $composition->anthologies()->attach($anthology->id);
                    }

                }

            }

            if (isset($fileAttributes['artist']['id'])) {
                $artist = Artist::where('id', $fileAttributes['artist']['id'])->first();
                $composition->artists()->attach($artist->id);
            } else {
                if ($previousArtist != $fileAttributes['artist']['name']) {
                    $artist = new Artist();
                    $artist->name = $fileAttributes['artist']['name'];
                    $artist->save();
                    $composition->artists()->attach($artist->id);
                } else {
                    $artist = Artist::where('name', $fileAttributes['artist']['name'])->first();
                    $composition->artists()->attach($artist->id);
                }

            }

            foreach ($fileAttributes['instruments'] as $instrumentName) {
                $instrument = Instrument::firstOrCreate($instrumentName);
                $composition->instruments()->attach($instrument->id);
            }

            if (isset($fileAttributes['tags'])) {
                foreach ($fileAttributes['tags'] as $tagName) {
                    $tag = Tag::firstOrCreate($tagName);
                    $composition->tags()->attach($tag->id);
                }
            }

            if (isset($fileAttributes['anthology'])) {
                $previousAnthology = $fileAttributes['anthology']['name'];
            }

            $previousArtist = $fileAttributes['artist']['name'];
        }

    }

}

