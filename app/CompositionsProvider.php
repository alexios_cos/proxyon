<?php


namespace App;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class CompositionsProvider
{
    private $storedCharacters = [
        0 => '_',
        1 => '<',
        2 => '>',
    ];

    private $publicCharacters = [
        0 => '.',
        1 => '"',
        2 => '"'
    ];

    public function getCompositionsByGenre(
        string $genre,
        array $filters,
        ?string $query,
        array $queryType,
        ?int $offset = 0,
        ?string $sorterColumn = 'name',
        ?string $sorterDirection = 'asc'
    ) {
        $queryBuilder = null;
        $genreModel = null;
        $subGenreModel = null;
        $compositions = null;
        $preparedCompositions = null;

        if ($genre == 'all categories') {
            $queryBuilder = Composition
                ::when($query, function ($queryOne, $query) use ($queryType){
                    $queryOne->when($queryType['composition'], function ($queryTwo) use ($query) {
                        return $queryTwo->where('name', 'like', '%' . $query . '%');
                    });
                    $queryOne->when($queryType['anthology'], function ($queryThree) use ($query) {
                        $queryThree->whereHas('anthologies', function (Builder $queryFive) use ($query) {
                            return $queryFive->where('name', 'like', '%' . $query . '%');
                        });
                    });
                    $queryOne->when($queryType['artist'], function ($queryFour) use ($query) {
                        $queryFour->whereHas('artists', function (Builder $querySix) use ($query) {
                            return $querySix->where('name', 'like', '%' . $query . '%');
                        });
                    });
                })
                ->when($filters, function ($queryFive, $filters) {
                    $queryFive->when($filters['tags'], function ($querySix) use ($filters){
                        $querySix->whereHas('tags', function (Builder $querySeven) use ($filters){
                            return $querySeven->whereIn('name', $filters['tags']);
                        });

                    });
                    $queryFive->when($filters['instruments'], function ($queryEight) use ($filters) {
                        $queryEight->whereHas('instruments', function (Builder $queryNine) use ($filters) {
                            return $queryNine->whereIn('name', $filters['instruments']);
                        });

                    });
                    $queryFive->when($filters['price'], function ($queryTen) use ($filters) {
                        return $queryTen->whereBetween('price', $filters['price']);
                    });
                });

            $preparedCompositions['attrs']['amount'] = $queryBuilder->count();
            $compositions = $queryBuilder
                ->orderBy($sorterColumn, $sorterDirection)
                ->offset($offset)
                ->limit(9)
                ->get();

        } else {
            $genreModel = Genre::where('name', $genre)->first();

            if ($genreModel) {
                $genreId = $genreModel->id;

                $queryBuilder = Composition
                    ::where('genre_id', $genreId)
                    ->when($query, function ($queryOne, $query) use ($queryType) {
                        $queryOne->when($queryType['composition'], function ($queryTwo) use ($query) {
                            return $queryTwo->where('name', 'like', '%' . $query . '%');
                        });
                        $queryOne->when($queryType['anthology'], function ($queryThree) use ($query) {
                            $queryThree->whereHas('anthologies', function (Builder $queryFive) use ($query) {
                                return $queryFive->where('name', 'like', '%' . $query . '%');
                            });
                        });
                        $queryOne->when($queryType['artist'], function ($queryFour) use ($query) {
                            $queryFour->whereHas('artists', function (Builder $querySix) use ($query) {
                                return $querySix->where('name', 'like', '%' . $query . '%');
                            });
                        });
                    })
                    ->when($filters, function ($queryFive, $filters) {
                        $queryFive->when($filters['tags'], function ($querySix) use ($filters){
                            $querySix->whereHas('tags', function (Builder $querySeven) use ($filters){
                                return $querySeven->whereIn('name', $filters['tags']);
                            });
                        });
                        $queryFive->when($filters['instruments'], function ($queryEight) use ($filters) {
                            $queryEight->whereHas('instruments', function (Builder $queryNine) use ($filters) {
                                return $queryNine->whereIn('name', $filters['instruments']);
                            });
                        });
                        $queryFive->when($filters['price'], function ($queryTen) use ($filters) {
                            return $queryTen->whereBetween('price', $filters['price']);
                        });
                    });

                $preparedCompositions['attrs']['amount'] = $queryBuilder->count();
                $compositions = $queryBuilder->orderBy($sorterColumn, $sorterDirection)
                    ->offset($offset)
                    ->limit(9)
                    ->with(['anthologies', 'artists'])
                    ->get();

            } else {
                $subGenreModel = SubGenre::where('name', $genre)->first();
                $subGenreId = $subGenreModel->id;

                $queryBuilder = Composition
                    ::where('sub_genre_id', $subGenreId)
                    ->when($query, function ($queryOne, $query) use ($queryType) {
                        $queryOne->when($queryType['composition'], function ($queryTwo) use ($query) {
                            return $queryTwo->where('name', 'like', '%' . $query . '%');
                        });
                        $queryOne->when($queryType['anthology'], function ($queryThree) use ($query) {
                            $queryThree->whereHas('anthologies', function (Builder $queryFive) use ($query) {
                                return $queryFive->where('name', 'like', '%' . $query . '%');
                            });
                        });
                        $queryOne->when($queryType['artist'], function ($queryFour) use ($query) {
                            $queryFour->whereHas('artists', function (Builder $querySix) use ($query) {
                                return $querySix->where('name', 'like', '%' . $query . '%');
                            });
                        });
                    })
                    ->when($filters, function ($queryFive, $filters) {
                        $queryFive->when($filters['tags'], function ($querySix) use ($filters){
                            $querySix->whereHas('tags', function (Builder $querySeven) use ($filters){
                                return $querySeven->whereIn('name', $filters['tags']);
                            });
                        });
                        $queryFive->when($filters['instruments'], function ($queryEight) use ($filters) {
                            $queryEight->whereHas('instruments', function (Builder $queryNine) use ($filters) {
                                return $queryNine->whereIn('name', $filters['instruments']);
                            });
                        });
                        $queryFive->when($filters['price'], function ($queryTen) use ($filters) {
                            return $queryTen->whereBetween('price', $filters['price']);
                        });
                    });

                $preparedCompositions['attrs']['amount'] = $queryBuilder->count();
                $compositions = $queryBuilder
                    ->orderBy($sorterColumn, $sorterDirection)
                    ->offset($offset)
                    ->limit(9)
                    ->with(['anthologies', 'artists'])
                    ->get();
            }

        }

        foreach ($compositions as $compositionIndex => $composition) {
            $preparedCompositions[$compositionIndex]['composition']['name'] = Str::limit(str_replace($this->storedCharacters, $this->publicCharacters, $composition->name), 37);

            if (isset($composition->anthologies[0]->name)) {
                $preparedCompositions[$compositionIndex]['anthology']['name'] = str_replace($this->storedCharacters, $this->publicCharacters, $composition->anthologies[0]->name);
            }

            $preparedCompositions[$compositionIndex]['artist']['name'] = str_replace($this->storedCharacters, $this->publicCharacters, $composition->artists[0]->name);
            $preparedCompositions[$compositionIndex]['composition']['id'] = $composition->id;
            $preparedCompositions[$compositionIndex]['composition']['marks'] = $composition->marks;
            $preparedCompositions[$compositionIndex]['composition']['price'] = $composition->price;
            $preparedCompositions[$compositionIndex]['composition']['sales'] = $composition->sales;
            $preparedCompositions[$compositionIndex]['composition']['path'] =  asset('storage/' . $composition->path);
            $preparedCompositions[$compositionIndex]['composition']['date'] = $composition->addition_date;

            if (isset($composition->license)) {
                $preparedCompositions[$compositionIndex]['composition']['license'] = $composition->license;
            }

        }

        $preparedCompositions['attrs']['genre-path'] = GenresProvider::getGenrePath($genre);
        $preparedCompositions['attrs']['pages'] = $this->countCompositionPages($preparedCompositions['attrs']['amount']);
        $preparedCompositions['attrs']['tags'] = $filters['tags'];
        $preparedCompositions['attrs']['instruments'] = $filters['instruments'];
        $preparedCompositions['attrs']['price'] = $filters['price'];

        if (isset($query)) {
            $preparedCompositions['attrs']['query'] = $query;

            if (isset($queryType['composition'])) {
                $preparedCompositions['attrs']['query-type'] = $queryType['composition'];
            } elseif (isset($queryType['anthology'])) {
                $preparedCompositions['attrs']['query-type'] = $queryType['anthology'];
            } else {
                $preparedCompositions['attrs']['query-type'] = $queryType['artist'];
            }

        } else {
            $preparedCompositions['attrs']['query'] = null;
            $preparedCompositions['attrs']['query-type'] = null;
        }

        return $preparedCompositions;
    }

    public function countCompositionsByGenre(string $genre)
    {
        $compositionsAmount = null;

        if ($genre == 'all categories') {
            $compositionsAmount = Composition::all()->count();
        } else {
            $genreModel = Genre::where('name', $genre)->first();

            if ($genreModel) {
                $compositionsAmount = $genreModel->compositions()->count();
            } else {
                $compositionsAmount = SubGenre::where('name', $genre)->first()->compositions()->count();
            }

        }

        return $compositionsAmount;
    }

    public function getMinCompositionPrice()
    {
        return Composition::min('price');
    }

    public function getMaxCompositionPrice()
    {
        return Composition::max('price');
    }

    public function countCompositionPages(int $compositionAmount)
    {
        $pagesAmount = null;
        $maxCompositionAmount = $this->countCompositionsByGenre('all categories');

        for ($n = 1; $n < $maxCompositionAmount / 9 + 1; $n++) {
            if ($compositionAmount - 9 * $n < 0 ) {
                $pagesAmount = $n;
                break;
            } else {
                continue;
            }

        }

        return $pagesAmount;
    }

}
