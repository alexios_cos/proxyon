<?php

namespace App\Console\Commands;


use App\CompositionsIntegrator;
use Illuminate\Console\Command;


class FileIntegrator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '
        file:integrate
        {filesFullPaths* : File\'s directory and complex of its attributes}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '
        Renames file (or each file in the array) with random string, 
        moves it to storage/app/public(if needed to set public access to file)/"directory name"/"sequence of directories each of which denominated with next single letter of file name starting from first one",
        inserts file information into database,
        shows updated file information in console;
        request example:
        "directory-name/
        tags(situational):tag1,tag2,tag3|
        instruments:instrument1,instrument2|
        genre:name=name|
        subgenre:name=name|
        artist:name=name;id(situational)=id-from-database|
        anthology(situational):name=name;id(situational)=id-from-database|
        compositions:price=price;name=name.extension";
        replaceable characters:
        . => _
        " => <
        " => >
    ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filesNames = null;
        $filesExtensions = null;
        $directoryName = null;
        $filesAttributes = null;

        $filesFullPaths = $this->argument('filesFullPaths');

        if (is_array($filesFullPaths)) {
            foreach ($filesFullPaths as $fileFullPath) {
                $fileParts = explode('/', $fileFullPath);
                $nameParts = explode('.', $fileParts[1]);
                $filesNames[] = $nameParts[0];
                $filesExtensions[] = $nameParts[1];
            }

            $directoryName = &$fileParts[0];

        } else {
            $fileParts = explode('/', $filesFullPaths);
            $directoryName = $fileParts[0];
            $nameParts = explode('.', $fileParts[1]);
            $filesNames = $nameParts[0];
            $filesExtensions = $nameParts[1];
        }

        switch ($directoryName) {
            case 'compositions':
                $compositionsIntegrator = new CompositionsIntegrator();
                $filesAttributes = $compositionsIntegrator->integrateCompositions($filesNames, $filesExtensions);

        }

        foreach ($filesAttributes as $fileIndex => $fileAttributes) {
            if (isset($fileAttributes['anthology'])) {
                $this->info(
                    implode(' + ', $fileAttributes['artist']) . ' | ' .
                    implode(' + ', $fileAttributes['anthology']) . ' | ' .
                    implode(' + ', $fileAttributes['composition'])
                );
            } else {
                $this->info(
                    implode(' + ', $fileAttributes['artist']) . ' | ' .
                    implode(' + ', $fileAttributes['composition'])
                );
            }

        }

    }

}
