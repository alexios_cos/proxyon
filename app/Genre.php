<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];

    public function compositions()
    {
        return $this->hasMany('App\Composition');
    }

    public function subGenres()
    {
        return $this->hasMany('App\SubGenre');
    }

}
