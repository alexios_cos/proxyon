<?php


namespace App;

class GenresProvider
{
    public function getGenreWithSubGenres($categoryName = 'all-categories')
    {
        $category = null;

        if ($categoryName == 'all-categories') {
            $category['category']['name'] = 'All categories';
            $category['category']['data-category'] = 'all-categories';
            $category['category']['content-amount'] = Composition::all()->count();
            $subCategories = Genre::orderBy('name')->get();

            foreach ($subCategories as $subCategoryIndex => $subCategory) {
                $category['subcategories'][$subCategoryIndex]['name'] = ucfirst($subCategory->name);
                $category['subcategories'][$subCategoryIndex]['data-category'] = str_replace(' ', '-', $subCategory->name);
                $category['subcategories'][$subCategoryIndex]['content-amount'] = $subCategory->compositions()->count();
            }

        } else {
            $genre = Genre::where('name', str_replace('-', ' ', $categoryName))->first();

            if (!isset($genre)) {
                $genre = SubGenre::where('name', str_replace('-', ' ', $categoryName))->first();
                $category['category']['data-category'] = str_replace(' ', '-', $genre->name);
                $category['category']['name'] = ucfirst($genre->name);
                $category['category']['content-amount'] = $genre->compositions()->count();
            } else {
                $category['category']['data-category'] = str_replace(' ', '-', $genre->name);
                $category['category']['name'] = ucfirst($genre->name);
                $category['category']['content-amount'] = $genre->compositions()->count();
                $subCategories = $genre->subGenres()->orderBy('name')->get();

                foreach ($subCategories as $subCategoryIndex => $subCategory) {
                    $category['subcategories'][$subCategoryIndex]['name'] = ucfirst($subCategory->name);
                    $category['subcategories'][$subCategoryIndex]['data-category'] = str_replace(' ', '-', $subCategory->name);
                    $category['subcategories'][$subCategoryIndex]['content-amount'] = $subCategory->compositions()->count();
                }

            }

        }

        return $category;
    }

    public function getAllGenresWithSubGenres()
    {
        $preparedGenres = null;

        $genres = Genre::with('subGenres')->get();

        foreach ($genres as $genreIndex => $genre) {
            $preparedGenres[$genreIndex]['name'] = ucfirst($genre->name);

            foreach ($genre->subGenres as $subGenreIndex => $subGenre) {
                $preparedGenres[$genreIndex]['sub-genres'][$subGenreIndex]['name'] = ucfirst($subGenre->name);
                $preparedGenres[$genreIndex]['sub-genres'][$subGenreIndex]['id'] = $subGenre->id;
            }
        }

        return $preparedGenres;
    }

    public static function getGenrePath($requestedGenre)
    {
        $genrePath = [
            'general' => 'All categories',
//            'genre' => null,
//            'subgenre' => null
        ];

        $genreModel = Genre::where('name', $requestedGenre)->first();

        if ($genreModel) {
            $genrePath['genre'] = ucfirst($genreModel->name);
        } else {
            $subGenreModel = SubGenre::where('name', $requestedGenre)->first();

            if ($subGenreModel) {
                $genreModel = $subGenreModel->genre()->first();
                $genrePath['genre'] = ucfirst($genreModel->name);
                $genrePath['subgenre'] = ucfirst($subGenreModel->name);
            }

        }

        return $genrePath;
    }

}
