<?php


namespace App\Http\Controllers;

use App\LibraryArticle;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    public function sendViewByRequest(Request $request)
    {
        $article = LibraryArticle::where('sub_genre_id', ucfirst($request->input('subGenreId')))->first();
        return view('article-library', ['header' => $article->header, 'content' => $article->content, 'source' => $article->source]);
    }

    public function sendViewByCategory($category = 'default')
    {
        if ($category == 'default') {
            return view('article-library-default');
        } else {
            $article = LibraryArticle::where('sub_genre_id', ucfirst($category))->first();
            return view('article-library', ['header' => $article->header, 'content' => $article->content, 'source' => $article->source]);
        }

    }

}
