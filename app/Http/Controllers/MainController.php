<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class MainController extends Controller
{
    public function sendView(Request $request)
    {
        $data = null;

        switch ($request->input('content')) {
            case "store":
                $storeController = new StoreController();

                if ($request->input('type')) {
                    $data = $storeController->sendViewByCategory($request->input('type'));
                } else {
                    $data = $storeController->sendViewByCategory();
                }

                break;
            case "library":
                $libraryController = new LibraryController();

                if ($request->input('type')) {
                    $data = $libraryController->sendViewByCategory($request->input('type'));
                } else {
                    $data = $libraryController->sendViewByCategory();
                }

                break;
            case "collection":
                $data = "";
                break;
        }

        return $data;
    }

}
