<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class SearchTypeButtonController extends Controller
{
    public function sendView(Request $request)
    {
        switch ($request->input('type')) {
            case 'composition':
                return view('anthology-artist-dropdown');
            case 'artist':
                return view('composition-anthology-dropdown');
            case 'anthology':
                return view('composition-artist-dropdown');
        }

    }

}
