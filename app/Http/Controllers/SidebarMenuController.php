<?php


namespace App\Http\Controllers;

use App\CompositionsProvider;
use App\GenresProvider;
use App\InstrumentsProvider;
use App\TagsProvider;
use Illuminate\Http\Request;

class SidebarMenuController extends Controller
{
    public function sendView(Request $request)
    {
        $sidebarName = $request->input('sidebarName');

        if ($request->session()->has("sidebarMenu.$sidebarName")) {
            $view = $request->session()->get("sidebarMenu.$sidebarName");
            return $view[0];
        } else {
            if ($sidebarName == 'store') {
                $genresProvider = new GenresProvider();
                $genres = $genresProvider->getGenreWithSubGenres();
                $tagsProvider = new TagsProvider();
                $tags = $tagsProvider->getAllTags();
                $instrumentsProvider = new InstrumentsProvider();
                $instruments = $instrumentsProvider->getAllInstruments();
                $compositionsProvider = new CompositionsProvider();
                $minPrice = $compositionsProvider->getMinCompositionPrice();
                $maxPrice = $compositionsProvider->getMaxCompositionPrice();
                return view($sidebarName . '-sidebar', ['categories' => $genres, 'tags' => $tags, 'instruments' => $instruments, 'maxPrice' => $maxPrice, 'minPrice' => $minPrice]);
            } elseif ($sidebarName == 'library') {
                $genresProvider = new GenresProvider();
                $genres = $genresProvider->getAllGenresWithSubGenres();
                return view($sidebarName . '-sidebar', ['genres' => $genres]);
            } else {
                return view($sidebarName . '-sidebar');
            }

        }

    }

    public function storeView(Request $request)
    {
        $sidebarName = $request->input('sidebarName');
        $request->session()->forget("sidebarMenu.$sidebarName");
        $request->session()->push("sidebarMenu.$sidebarName", $request->input('sidebarView'));
    }

    public function eraseStoredViews(Request $request)
    {
        $sessionContainer = $request->input('value');

        if ($request->session()->has($sessionContainer)) {
            $request->session()->forget($sessionContainer);
        }

    }

}
