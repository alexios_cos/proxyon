<?php


namespace App\Http\Controllers;


use App\CompositionsProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class StoreController extends Controller
{
    public function sendViewByRequest(Request $request)
    {
        $offset = null;
        $filters = [
            'tags' => null,
            'instruments' => null,
            'price' => null
        ];
        $compositionsAttrs = null;
        $sorterColumn = null;
        $sorterDirection = null;
        $userQuery = null;
        $queryType = [
            'composition' => null,
            'anthology' => null,
            'artist' => null
        ];

        $compositionsProvider = new CompositionsProvider();
        $category = str_replace('-', ' ', $request->input('category'));

        if ($request->input('music')) {
            $musicalFilters = $request->input('music');
            if (isset($musicalFilters['tags'])) {
                $filters['tags'] = $musicalFilters['tags'];
                $filters['tags'] = str_replace(',', ' ', $filters['tags']);
                $filters['tags'] = trim($filters['tags']);
                $filters['tags'] = explode(' ', $filters['tags']);

                foreach ($filters['tags'] as &$tag) {
                    $tag = str_replace('undefined', '', $tag);
                    $tag = str_replace('-', ' ', $tag);
                }

            }

            if (isset($musicalFilters['instruments'])) {
                $filters['instruments'] = $musicalFilters['instruments'];
                $filters['instruments'] = str_replace(',', ' ', $filters['instruments']);
                $filters['instruments'] = trim($filters['instruments']);
                $filters['instruments'] = explode(' ', $filters['instruments']);

                foreach ($filters['instruments'] as &$instrument) {
                    $instrument = str_replace('undefined', '', $instrument);
                    $instrument = str_replace('-', ' ', $instrument);
                }

            }

        }

        if ($request->input('price')) {
            $filters['price'] = $request->input('price');

            if (!isset($filters['price']['minPrice'])) {
                $filters['price']['minPrice'] = $compositionsProvider->getMinCompositionPrice();
            }

            if (!isset($filters['price']['maxPrice'])) {
                $filters['price']['maxPrice'] = $compositionsProvider->getMaxCompositionPrice();
            }

        }

        if ($request->input('offset')) {
            if ($request->input('offset') == 'zero') {
                $offset = '0';
            } else {
                $offset = $request->input('offset');
            }

        }

        if ($request->input('sorter')) {
            $sorter = $request->input('sorter');
            $sorter = explode(',', $sorter);
            $sorterColumn = $sorter[0];
            $sorterDirection = $sorter[1];
        }

        if ($request->input('userQuery')) {
            $userQuery = $request->input('userQuery');
        }

        if ($request->input('queryType')) {
            switch ($request->input('queryType')) {
                case 'composition':
                    $queryType['composition'] = 'composition';
                break;
                case 'anthology':
                    $queryType['anthology'] = 'anthology';
                break;
                case 'artist':
                    $queryType['artist'] = 'artist';
                break;
            }

        }

        if (isset($sorterColumn)) {
            $compositions = $compositionsProvider->getCompositionsByGenre($category, $filters, $userQuery, $queryType, $offset, $sorterColumn, $sorterDirection);
        } else {
            $compositions = $compositionsProvider->getCompositionsByGenre($category, $filters, $userQuery, $queryType, $offset);
        }

        $compositionsAttrs = $this->prepareTagsAndInstruments($compositions['attrs']);
        unset($compositions['attrs']);

        if ($offset == '0') {
            $offset = 'zero';
        }

        if ($offset) {
            return view('compositions-main-page', ['compositions' => $compositions, 'compositionsAttrs' => $compositionsAttrs]);
        } else {
            if (isset($compositions[0])) {
                if ($sorterColumn) {
                    $response['compositions'] = View::make(
                        'compositions-main-page',
                        ['compositions' => $compositions, 'compositionsAttrs' => $compositionsAttrs]
                    )->render();
                    $response['pages'] = View::make('compositions-main-pages', ['compositionsAttrs' => $compositionsAttrs])->render();
                    return response()->json($response);
                } else {
                    return view('compositions-main', ['compositions' => $compositions, 'compositionsAttrs' => $compositionsAttrs]);
                }

            } else {
                return view('compositions-main-empty', ['compositionsAttrs' => $compositionsAttrs]);
            }

        }

    }

    public function sendViewByCategory($category = 'all categories')
    {
        $filters = [
            'tags' => null,
            'instruments' => null,
            'price' => null
        ];
        $offset = null;
        $userQuery = '';
        $queryType = [
            'composition' => null,
            'anthology' => null,
            'artist' => null
        ];

        $category = str_replace('-', ' ', $category);

        $compositionsProvider = new CompositionsProvider();
        $compositions = $compositionsProvider->getCompositionsByGenre($category, $filters, $userQuery, $queryType, $offset);

        $compositionsAttrs = $this->prepareTagsAndInstruments($compositions['attrs']);
        unset($compositions['attrs']);

        return view('compositions-main', ['compositions' => $compositions, 'compositionsAttrs' => $compositionsAttrs]);

    }

    protected function prepareTagsAndInstruments($compositionsAttrs)
    {
        if (isset($compositionsAttrs['tags'])) {
            foreach ($compositionsAttrs['tags'] as &$tag) {
                $tag = [
                    'space' => $tag,
                    'kebab' => Str::kebab($tag)
                ];
            }

        }

        if (isset($compositionsAttrs['instruments'])) {
            foreach ($compositionsAttrs['instruments'] as &$instrument) {
                $instrument = [
                    'space' => $instrument,
                    'kebab' => Str::kebab($instrument)
                ];
            }

        }

        return $compositionsAttrs;
    }

}
