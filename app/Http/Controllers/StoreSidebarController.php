<?php


namespace App\Http\Controllers;


use App\GenresProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class StoreSidebarController extends Controller
{
    public function sendView(Request $request)
    {
        $genresProvider = new GenresProvider();
        $categories = $genresProvider->getGenreWithSubGenres($request->input('category'));
        $response['categoryButton'] = View::make('category-button', ['category' => $categories['category']])->render();

        if (isset($categories['subcategories'])) {
            $response['subCategories'] = View::make('sub-category-button', ['subCategories' => $categories['subcategories']])->render();
        }

        return response()->json($response);
    }

}
