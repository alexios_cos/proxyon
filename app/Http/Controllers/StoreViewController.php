<?php


namespace App\Http\Controllers;


use App\CompositionsProvider;
use App\GenresProvider;
use App\InstrumentsProvider;
use App\TagsProvider;
use Illuminate\Support\Str;

class StoreViewController extends Controller
{
    public function sendView()
    {
        $category = 'all categories';
        $offset = null;
        $filters = [
            'tags' => null,
            'instruments' => null,
            'price' => null
        ];
        $offset = null;
        $userQuery = null;
        $queryType = [
            'composition' => null,
            'anthology' => null,
            'artist' => null
        ];

        $genresProvider = new GenresProvider();
        $categories = $genresProvider->getGenreWithSubGenres();
        $tagsProvider = new TagsProvider();
        $tags = $tagsProvider->getAllTags();
        $instrumentsProvider = new InstrumentsProvider();
        $instruments = $instrumentsProvider->getAllInstruments();
        $compositionsProvider = new CompositionsProvider();
        $minPrice = $compositionsProvider->getMinCompositionPrice();
        $maxPrice = $compositionsProvider->getMaxCompositionPrice();

        $compositionsProvider = new CompositionsProvider();
        $compositions = $compositionsProvider->getCompositionsByGenre($category, $filters, $userQuery, $queryType, $offset);
        $compositionsAttrs = array_shift($compositions);

        if (isset($compositionsAttrs['tags'])) {
            foreach ($compositionsAttrs['tags'] as &$tag) {
                $tag = [
                    'space' => $tag,
                    'kebab' => Str::kebab($tag)
                ];
            }

        }

        if (isset($compositionsAttrs['instruments'])) {
            foreach ($compositionsAttrs['instruments'] as &$instrument) {
                $instrument = [
                    'space' => $instrument,
                    'kebab' => Str::kebab($instrument)
                ];
            }

        }

        return view(
            'store', [
                'categories' => $categories,
                'tags' => $tags,
                'instruments' => $instruments,
                'maxPrice' => $maxPrice,
                'minPrice' => $minPrice,
                'compositions' => $compositions,
                'compositionsAttrs' => $compositionsAttrs
            ]
        );
    }

}
