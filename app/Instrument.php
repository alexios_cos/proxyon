<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Instrument extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];

    public function compositions()
    {
        return $this->belongsToMany('App\Composition', 'music_parts');
    }

}
