<?php


namespace App;


use Illuminate\Support\Str;

class InstrumentsProvider
{
    public function getAllInstruments()
    {
        $instrumentsNames = null;
        $instruments = Instrument::orderBy('name')->get();

        foreach ($instruments as $instrumentIndex => $instrument) {
            $instrumentsNames[$instrumentIndex]['name'] = [
                'space' => $instrument->name,
                'kebab' => Str::kebab($instrument->name)
            ];
        }

        return $instrumentsNames;
    }

}
