<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryArticle extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function subGenre()
    {
        return $this->belongsTo('App\SubGenre');
    }

}
