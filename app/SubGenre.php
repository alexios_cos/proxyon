<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class SubGenre extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];

    public function compositions()
    {
        return $this->hasMany('App\Composition');
    }

    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }

    public function libraryArticle()
    {
        return $this->hasOne('App\LibraryArticle');
    }

}
