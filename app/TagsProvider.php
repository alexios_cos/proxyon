<?php


namespace App;


use Illuminate\Support\Str;

class TagsProvider
{
    public function getAllTags()
    {
        $tagsNames = null;
        $tags = Tag::orderBy('name')->get();

        foreach ($tags as $tagIndex => $tag) {
            $tagsNames[$tagIndex]['name'] = [
                'space' => $tag->name,
                'kebab' => Str::kebab($tag->name)
            ];
//            $tagsNames[$tagIndex]['name'][0] = $tag->name;
//            $tagWords = explode(' ', $tag->name);
//            if (count($tagWords) > 1) {
//                $tagsNames[$tagIndex]['name'][1] = Str::kebab($tag->name);
//            } else {
//                $tagsNames[$tagIndex]['name'][1] = $tag->name;
//            }

        }

        return $tagsNames;
    }

}
