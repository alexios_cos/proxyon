/**
 * Gets from server said sidebar template, corresponding main template and renders it. Shows or hides said sidebar.
 * Makes ajax navigation through the web app.
 */
$( function () {
    var load = function (url) {
        var tab = $( "a[href=" + url + "]" );

        if (!$("#sidebar-menu").children().attr("class")) {
            $.ajax({
                url: "/sidebarMenuAjaxDirective",
                data: { value: "sidebarMenu" },
                async: false
            });

            $.ajax({
                url: "/sidebarMenuAjaxRequest",
                data: { sidebarName: url },
                success: function (response) {
                    $(tab).addClass("show");
                    $("#sidebar-menu").html(response);
                    $("#sidebar-menu").fadeIn(100);
                }

            });

            $.ajax( {
                url: "/mainAjaxRequest",
                data: { content: url },
                success: function ( response ) {
                    $( "#main-container" ).fadeOut( 100, function () {
                        $( "#main" ).html( response );
                        $( "#main-container" ).fadeIn( 100 );
                    } );
                }
            } );

        } else {
            $.ajax({
                url: "/sidebarMenuAjaxRequest",
                data: { sidebarName: url },
                async: false,
                success: function (response) {
                    if ($("#sidebar-menu").children().attr("data-sidebar") !== $(response).attr("data-sidebar")) {
                        var sidebarPackage = "#sidebar-menu";

                        if ($(sidebarPackage).css("display") !== "none") {
                            $(sidebarPackage).find(".sidebar-list-content").removeClass("invisible");
                            $(sidebarPackage).find(".sidebar-list-content").addClass("visible");
                        }

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: "POST",
                            url: "/sidebarMenuAjaxDelivery",
                            data: { sidebarView: $(sidebarPackage).html(), sidebarName: $("#sidebar-menu").children().attr("data-sidebar") },
                            async: false
                        });

                        if ($("#sidebar-menu").css("display") !== "none") {
                            $("#nav-menu .nav-tab").removeClass("show");
                            $(tab).addClass("show");
                            $("#sidebar-menu").find(".sidebar-list-content").removeClass("invisible");
                            $("#sidebar-menu").find(".sidebar-list-content").addClass("visible");
                            $("#sidebar-menu").find(".sidebar-list-content").fadeOut(100, function () {
                                $("#sidebar-menu").html(response);
                                $("#sidebar-menu").find(".sidebar-list-content").fadeIn(100);
                            });

                        } else {
                            $("#nav-menu .nav-tab").removeClass("show");
                            $(tab).addClass("show");
                            $("#sidebar-menu").html(response);
                            $("#sidebar-menu").fadeIn(100);

                        }

                    } else {
                        if ($(tab).hasClass("show")) {
                            $(tab).removeClass("show");
                            $("#sidebar-menu").fadeOut(100);
                        } else {
                            $(tab).addClass("show");
                            $("#sidebar-menu").fadeIn(100);
                        }

                    }
                }

            });

            var currentOption;

            switch (url) {
                case 'store':
                    currentOption = window.currentStoreOption;
                    break;
                case 'library':
                    currentOption = window.currentLibraryOption;
                    break;
            }

            var content = $( "#main" ).children().attr( "data-content" );

            $.ajax( {
                url: "/mainAjaxRequest",
                data: { content: url, type: currentOption },
                success: function (response) {
                    if ( $( response ).attr( "data-content" ) !== content ) {
                        $( "#main-container" ).fadeOut( 100, function () {
                            $( "#main" ).html( response );
                            $( "#main-container" ).fadeIn(100);
                        } );

                    }

                }

            } );

        }

    };

    $(document).on( 'click', '#nav-menu a', function (e) {
        e.preventDefault();

        var $this = $(this),
            url = $this.attr("href"),
            title = url.charAt( 0 ).toUpperCase() + url.substr( 1 );

        history.pushState({
            url: url,
            title: title
        }, title, url);

        document.title = title;
        load(url);
    });

    $(window).on('popstate', function (e) {
        var state = e.originalEvent.state;
        if (state !== null) {
            document.title = state.title;
            load(state.url);
        } else {
            document.title = 'Welcome';
            $("#sidebar-menu").empty();
            $.ajax( {
                url: "/welcomeAjaxRequest",
                success: function (response) {
                    $( "#main" ).html(response);
                }
            } );
        }
    });

} );

/**
 * Transforms accordion section
 */
$("body").on("click", ".accordion-button", function () {

    if ($(this).hasClass("on")) {
        $(this).removeClass("on");
        $(this).addClass("off");
        $(this).siblings().slideUp(200);
    } else {
        $(this).removeClass("off");
        $(this).addClass("on");
        $(this).siblings().slideDown(200);
    }

});

/**
 * Shows or hides said sidebar utilities
 */
$("#nav-utilities .nav-tab").on("click", function () {

    if ($(this).hasClass("show")) {
        $(this).removeClass("show");
        $('#sidebar-utilities').fadeOut(100);

    } else {
        if ($("#nav-utilities .nav-tab").hasClass("show")) {
            $("#nav-utilities .nav-tab").removeClass("show");
        }

        $(this).addClass("show");
        $('#sidebar-utilities').fadeIn(100);
    }

});

/**
 * Gets from server library's genre article and renders it.
 */
$("body").on("click", ".sub-genre-name", function () {
    window.currentLibraryOption = $( this ).attr( "data-category" );
    var template = null;
    $.ajax({
        url: "/libraryAjaxRequest",
        data: { subGenreId: $(this).attr("data-category") },
        async: false
    }).done( function (response) {
        template = response;
    });

    if (!$("#main").children().attr("class")) {
        $(this).addClass("active");
        $("#main").html(template);
        $("#main").children().fadeIn(200);

    } else {
        if ($("#main").find(".library-article-sections").attr("data-article") !== $(template).find(".library-article-sections").attr("data-article")) {
            $(".sub-genre-name").removeClass("active");
            $(".sub-genre-name").addClass("inactive");
            $(this).removeClass("inactive");
            $(this).addClass("active");
            $("#main").children().fadeOut(200, function () {
                $("#main").html(template);
                $("#main").children().fadeIn(200);
            });
        }
    }
});

/**
 * Gets from server said store category button and its subcategories and renders it by addition/replacement. Moves the user one step further.
 */
$("body").on("click", ".sub-category-button", function () {
    // if (!$(".category-button").hasClass("back")) {
        $(".category-button").addClass("back");
    // }
    window.currentStoreOption = $( this ).attr( "data-category" );

        if ( $( ".category-button" ).hasClass( "current" ) ) {
            $( ".category-button" ).removeClass( "current" );
        }

    $.ajax({
        url: "/storeSidebarAjaxRequest",
        data: { category: $(this).attr("data-category") },
        success: function (response) {
            $(".sidebar-list").find(".categories").append(response.categoryButton);

            if (response.subCategories) {
                $(".sidebar-list").find(".sub-categories").replaceWith(response.subCategories);
            } else {
                $(".sidebar-list").find(".sub-category-button").remove();
            }

        }

    });

    var musicalFilters = currentCheckboxesValues();
    var priceFilters = currentPriceValues();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/storeAjaxRequest",
        type: "POST",
        data: {
            category: $( this ).attr( "data-category" ),
            music: musicalFilters,
            price: priceFilters
        },
        success: function (response) {
            // $( ".compositions-container" ).fadeOut(100, function () {
            //     $( "#main" ).html(response);
            //     $( ".compositions-container" ).fadeIn(100);
            // });
            $( "#main" ).html(response);
        }
    });

});

/**
 * Gets from server subcategories of said store category button and renders it by replacement. Moves the user back.
 */
$("body").on("click", ".category-button.back", function () {
    window.currentStoreOption = $( this ).attr( "data-category" );

    if ( $( this ).hasClass( "back" ) ) {
        $( this ).removeClass( "back" );
        $( this ).addClass( "current" )
    }

    $(this).nextAll(".category-button").remove();

    $.ajax({
        url: "/storeSidebarAjaxRequest",
        data: { category: $(this).attr("data-category") },
        success: function (response) {
            if (response.subCategories) {
                $( ".sub-categories-container" ).fadeOut( 100, function () {
                    $(".sub-categories").replaceWith(response.subCategories);
                    $( ".sub-categories-container" ).fadeIn( 100 );
                });
                // $( ".sub-categories-container" ).hide( 200, function () {
                //     $(".sub-categories").replaceWith(response.subCategories);
                //     $( ".sub-categories-container" ).show( 200 );
                // });
                // $(".sidebar-list").find(".sub-categories").replaceWith(response.subCategories);
            } else {
                $(".sidebar-list").find(".sub-category-button").remove();
            }
        }

    });



    var musicalFilters = currentCheckboxesValues();
    var priceFilters = currentPriceValues();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/storeAjaxRequest",
        type: "POST",
        data: {
            category: $( this ).attr( "data-category" ),
            music: musicalFilters,
            price: priceFilters
        },
        success: function (response) {
            // $( ".compositions-container" ).fadeOut(100, function () {
            //     $( "#main" ).html(response);
            //     $( ".compositions-container" ).fadeIn(100);
            // });
            $( "#main" ).html(response);
        }
    });

});

var currentCheckboxesValues = function getCheckboxesValues() {
    var values = {};

    $( "input[class=checkbox-input]:checked" ).each( function (index) {
        if ( $( this ).attr( "data-type" ) === "tag" ) {
            values.tags += $( this ).val() + ',';
        } else {
            values.instruments += $( this ).val() + ',';
        }

    } );

    return values;
};

var currentPriceValues = function getPriceValues() {
    var values = {};

    $( "input[class=price-input]" ).each( function (index) {
        if ( $( this ).attr( "id" ) === "min-price" ) {
            if ( $ (this).val() ) {
                values.minPrice = $( this ).val();
            }

        } else {
            if ( $ (this).val() ) {
                values.maxPrice = $( this ).val();
            }

        }

    } );
    return values;
};

/**
 * Part of "Filters". See below
 */
var filteredCompositions = function getFilteredCompositions() {
    var musicalFilters = currentCheckboxesValues();
    var priceFilters = currentPriceValues();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/storeAjaxRequest",
        type: "POST",
        data: {
            category: $( ".category-button.current" ).attr( "data-category" ),
            music: musicalFilters,
            price: priceFilters
        },
        success: function (response) {
            $( "#main" ).html(response);
        }

    });

};

/**
 * "Filters". Gets all non-category filters and sends it to server. Renders corresponding compositions
 */
$( "body" ).on( "mouseup", ".checkbox-label", filteredCompositions );
$( "body" ).on( "mouseup", ".checkbox-input", filteredCompositions );
$( "body" ).on( "mouseup", ".price-button", filteredCompositions );

/**
 * Check-transformer
 */
$( "body" ).on( "mousedown", ".checkbox-label", function () {
    if ( $( this ).siblings().is( ":checked" ) ) {
        $( this ).siblings().prop( "checked", false );
    } else {
        $( this ).siblings().prop( "checked", true );
    }

});

/**
 * Check-transformer
 */
$( "body" ).on( "mousedown", ".checkbox-input", function () {
    if ( $( this ).is( ":checked" ) ) {
        $( this ).prop( "checked", false );
    } else {
        $( this ).prop( "checked", true );
    }

});

/**
 * Bundle of filter clear buttons
 */
$( "body" ).on( "mousedown", ".tag-filter", function () {
    var tag = $( this ).attr( "data-name" );
    $( "#" + tag + "-checkbox" ).prop( "checked", false );
} );

$( "body" ).on( "mouseup", ".tag-filter", filteredCompositions );

$( "body" ).on( "mousedown", ".instrument-filter", function () {
    var instrument = $( this ).attr( "data-name" );
    $( "#" + instrument + "-checkbox" ).prop( "checked", false );
} );

$( "body" ).on( "mouseup", ".instrument-filter", filteredCompositions );

$( "body" ).on( "mousedown", ".prices-filter", function () {
    $( ".price-input" ).val( null );
} );

$( "body" ).on( "mouseup", ".prices-filter", filteredCompositions );

$( "body" ).on( "click", ".request-filter", filteredCompositions );

/**
 * "Clear all filters" button
 */
$( "body" ).on( "mousedown", ".clear-all-filters", function () {
    $( ".checkbox-input" ).prop( "checked", false );
    $( ".price-input" ).val( null );
} );

$( "body" ).on( "mouseup", ".clear-all-filters", function () {

    $.ajax({
        url: "/storeAjaxRequest",
        data: { category: $( ".category-button.current" ).attr( "data-category" ) },
        success: function (response) {
            $( "#main" ).html(response);
        }

    });

} );

/**
 * Gets from server composition search type dropdown list and renders it. Opens/closes the dropdown.
 */
$( "body" ).on( "click", ".composition-search-type", function () {
    if ( $( ".search-icons-dropdown" ).attr( "data-type" ) === $( ".dropdown-icon.current" ).attr( "data-type" ) ) {
        if ( $( ".search-icons-dropdown" ).css( "display" ) === "none" ) {
            $ ( ".dropdown-icon.current" ).css( "border-color", "#e0e0e0" );
            $ ( ".dropdown-icon.current" ).css( "box-shadow", "0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)" );
            $ ( ".composition-search-type-button" ).removeClass( "off" );
            $ ( ".composition-search-type-button" ).addClass( "on" );
            $( ".search-icons-dropdown" ).show();
        } else {
            $ ( ".dropdown-icon.current" ).css( "box-shadow", "none" );
            $ ( ".dropdown-icon.current" ).css( "border-color", "transparent" );
            $ ( ".composition-search-type-button" ).removeClass( "on" );
            $ ( ".composition-search-type-button" ).addClass( "off" );
            $( ".search-icons-dropdown" ).hide();
        }

    } else {
        $( ".search-icons-dropdown" ).remove();
        $.ajax( {
            url: "/searchTypeButtonAjaxRequest",
            data: { type: $( ".dropdown-icon.current" ).attr( "data-type" ) },
            success: function (response) {
                $ ( ".dropdown-icon.current" ).css( "box-shadow", "0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)" );
                $ ( ".dropdown-icon.current" ).css( "border-color", "#e0e0e0" );
                $ ( ".composition-search" ).after( response );
                $ ( ".composition-search-type-button" ).removeClass( "off" );
                $ ( ".composition-search-type-button" ).addClass( "on" );
                $(".search-icons-dropdown").show();
            }

        } );

    }

} );

// /**
//  * Hides search icons dropdown when clicking out of the element.
//  */
// $( "body" ).mouseup( function (e) {
//     var container = $( ".search-icons-dropdown" );
//     if ( !container.is(e.target) ){
//         $ ( ".dropdown-icon.current" ).css( "box-shadow", "none" );
//         $ ( ".dropdown-icon.current" ).css( "border-color", "transparent" );
//         $ ( ".composition-search-type-button" ).removeClass( "on" );
//         $ ( ".composition-search-type-button" ).addClass( "off" );
//         container.hide();
//     }
//
// } );

/**
 * Picks said dropdown icon and replaces current icon with it.
 */
$( "body" ).on( "click", ".dropdown-icon.option", function () {
    $( ".dropdown-icon.current" ).replaceWith( $( this ) );
    $( this ).removeClass( "option" );
    $( this ).removeClass( "first" );
    $( this ).addClass( "current" );
    $ ( ".composition-search-type-button" ).removeClass( "on" );
    $ ( ".composition-search-type-button" ).addClass( "off" );
    $( ".search-icons-dropdown" ).hide();
} );

/**
 * Sends to server user's search request within already found compositions. Renders the result.
 */
$( "body" ).on( "click", "#composition-search-button", function () {
    var musicalFilters = currentCheckboxesValues();
    var priceFilters = currentPriceValues();

    $.ajax( {
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/storeAjaxRequest",
        type: "POST",
        data: {
            category: $( ".category-button.current" ).attr( "data-category" ),
            music: musicalFilters,
            price: priceFilters,
            userQuery: $( "#composition-search-input" ).val(),
            queryType: $( ".dropdown-icon.current" ).attr( "data-type" )
        },
        success: function (response) {
            $( "#main" ).html(response);
        }
    } );

} );

/**
 * Moves to said page with compositions
 */
$( "body" ).on( "click", ".page-number", function () {
    $( ".page-number" ).removeClass( "active" );
    $( this ).addClass( "active" );
    // $( "#page-next" ).attr( "data-offset", parseInt($( this ).attr( "data-offset" ), 10) + 9 );

    var musicalFilters = currentCheckboxesValues();
    var priceFilters = currentPriceValues();

    $.ajax( {
        url: "/storeAjaxRequest",
        data: {
            category: $( ".category-button.current" ).attr( "data-category" ),
            music: musicalFilters,
            price: priceFilters,
            userQuery: $( ".user-query" ).attr( "data-query" ),
            queryType: $( ".user-query" ).attr( "data-query-type" ),
            offset: $( this ).attr( "data-offset" ),
            sorter: $( ".sorter.current" ).attr( "data-sort" ) + "," + $( ".sorter.current" ).attr( "data-sort-direction-current" )
        },
        success: function (response) {
            $( ".compositions-container" ).fadeOut(100, function () {
                $( ".compositions" ).html(response);
                $( ".compositions-container" ).fadeIn(100);
            })
        }

    } );

} );

/**
 * Set said sorter and renders compositions in corresponding order
 */
$( "body" ).on( "click", ".sorter", function () {
    var musicalFilters = currentCheckboxesValues();
    var priceFilters = currentPriceValues();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/storeAjaxRequest",
        type: "POST",
        data: {
            category: $( ".category-button.current" ).attr( "data-category" ),
            music: musicalFilters,
            price: priceFilters,
            userQuery: $( ".user-query" ).attr( "data-query" ),
            queryType: $( ".user-query" ).attr( "data-query-type" ),
            sorter: $( this ).attr( "data-sort" ) + "," + $( this ).attr( "data-sort-direction-next" )
        },
        success: function (response) {
            $( ".compositions" ).html(response.compositions);
            $( ".composition-pages" ).replaceWith(response.pages);
        }
    });

    if ( $( this ).hasClass( "current" ) ) {
        if ( $( this ).attr( "data-sort-direction-next" ) === 'asc' ) {
            $( this ).attr( "data-sort-direction-next", 'desc' );
            $( this ).attr( "data-sort-direction-current", 'asc' );
            $( this ).find( ".sort-sign" ).html("&#8657;");
        } else {
            $( this ).attr( "data-sort-direction-next", 'asc' );
            $( this ).attr( "data-sort-direction-current", 'desc' );
            $( this ).find( ".sort-sign" ).html("&#8659;");
        }

    } else {
        $( ".sorter.current" ).addClass( "basic" );
        $( ".sorter.current" ).find( ".sort-sign" ).html("&#8652;");
        $( ".sorter.current" ).attr( "data-sort-direction-next", 'asc' );
        $( ".sorter.current" ).removeAttr( "data-sort-direction-current" );
        $( ".sorter.current" ).removeClass( "current" );

        $( this ).addClass( "current" );
        $( this ).removeClass( "basic" );
        $( this ).attr( "data-sort-direction-next", 'desc' );
        $( this ).attr( "data-sort-direction-current", 'asc' );
        $( this ).find( ".sort-sign" ).html("&#8657;");
    }

} );

/**
 * Transforms search input on main compositions page
 */
$( "body" ).on( "mouseenter", ".composition-search", function () {
    $( this ).css( "border-color", "#2a3244" );

    if ( $( "#composition-search-input" ).val() ) {
        $( "#composition-search-button" ).find( "i" ).css( "color", "#2a3244" );
    }

} );

/**
 * Transforms search input on main compositions page
 */
$( "body" ).on( "mouseleave", ".composition-search", function () {
    if ( $( "#composition-search-input" ).is( ":focus" ) ||  $( "#composition-search-input" ).val() ) {
        $( ".composition-search" ).css( "border-color", "#2a3244" );

        if ( $( "#composition-search-input" ).val() ) {
            $( "#composition-search-button" ).find( "i" ).css( "color", "#2a3244" );
        } else {
            $( "#composition-search-button" ).find( "i" ).css( "color", "#e0e0e0" );
        }

    } else {
        $( this ).css( "border-color", "#e0e0e0" );
        $( "#composition-search-button" ).find( "i" ).css( "color", "#e0e0e0" );
    }

} );

/**
 * Transforms search input on main compositions page
 */
$( "body" ).on( "focusin", "#composition-search-input", function () {
    $( ".composition-search" ).css( "border-color", "#2a3244" );

    if ( $( this ).val() ) {
        $( "#composition-search-button" ).find( "i" ).css( "color", "#2a3244" );
    }

} );

/**
 * Transforms search input on main compositions page
 */
$( "body" ).on( "focusout", "#composition-search-input", function () {
    if ( $( this ).val() ) {

    } else {
        $( ".composition-search" ).css( "border-color", "#e0e0e0" );
        $( "#composition-search-button" ).find( "i" ).css( "color", "#e0e0e0" );
    }

} );

/**
 * Transforms search input on main compositions page
 */
$( "body" ).on( "keyup", "#composition-search-input", function () {
    if ( $( this ).val().length >= 1 ) {
        $( "#composition-search-button" ).find( "i" ).css( "color", "#2a3244" );
    } else {
        $( "#composition-search-button" ).find( "i" ).css( "color", "#e0e0e0" );
    }

} );
