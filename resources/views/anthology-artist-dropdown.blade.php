<div class="search-icons-dropdown" data-type="composition">
    <div class="dropdown-icon option anthology-type" title="Anthology" data-type="anthology"><span>&#119070;</span></div>
    <div class="dropdown-icon option first artist-type" title="Artist" data-type="artist"><span>&#9998;</span></div>
</div>
