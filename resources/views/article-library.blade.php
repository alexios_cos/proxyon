<div class="main-container" data-content="library">
    <div class="main-content">
        <div class="library-article">
            <div class="library-article-header-container">
                <div class="library-article-header">{{ $header }}</div>
            </div>
            <div class="library-article-sections-container">{!! $content !!}</div>
            <div class="library-article-source"><span class="faded-legend">Source:</span> {{ $source }}</div>
        </div>
    </div>
</div>
