<div class="category-button current" data-category="{{ $category['data-category'] }}">
    <div class="back-sign"><span>&#9668;</span></div>
    <div class="category-name">{{ $category['name'] }}</div>
    <div class="category-content-amount">{{ $category['content-amount'] }}</div>
</div>
