<div class="sidebar-list" data-sidebar="collection">
    <div class="sidebar-list-content invisible">
        <div class="no-content-info">
            <div>Oops... Somebody has stolen the content! We will try to find it.</div>
        </div>
    </div>
</div>
