<div class="search-icons-dropdown" data-type="artist">
    <div class="dropdown-icon option composition-type" title="Composition" data-type="composition"><span>&#9836;</span></div>
    <div class="dropdown-icon option first anthology-type" title="Anthology" data-type="anthology"><span>&#119070;</span></div>
</div>
