<div class="search-icons-dropdown" data-type="anthology">
    <div class="dropdown-icon option composition-type" title="Composition" data-type="composition"><span>&#9836;</span></div>
    <div class="dropdown-icon option first artist-type" title="Artist" data-type="artist"><span>&#9998;</span></div>
</div>
