@foreach ($compositions as $composition)
    <div class="composition" data-id="{{ $composition['composition']['id'] }}">
        <audio src="{{ $composition['composition']['path'] }}" controls="controls"></audio>
        <div class="composition-info">
            <div class="composition-name composition-attr"><span>{{ $composition['composition']['name'] }}</span></div>
            @if (isset($composition['anthology']['name']))
                <div class="composition-anthology composition-attr"><i>from </i>{{ $composition['anthology']['name'] }}</div>
            @endif
            <div class="composition-artist composition-attr"><i>by </i>{{ $composition['artist']['name'] }}</div>
        </div>
        <div class="composition-features">
            <div class="composition-marks composition-feature">{{ $composition['composition']['marks'] }} marks</div>
            <div class="composition-sales composition-feature">{{ $composition['composition']['sales'] }} sales</div>
            <div class="composition-price composition-feature">{{ $composition['composition']['price'] }}$</div>
        </div>
        <div class="composition-actions">
            <div class="composition-mark composition-action">&#9733;</div>
            <div class="composition-wish composition-action">&#10084;</div>
            <div class="composition-buy composition-action">&#36;</div>
        </div>
    </div>
@endforeach
