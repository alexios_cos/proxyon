<div class="composition-pages">
    @if ($compositionsAttrs['pages'] > 1)
        @for ($i = 1; $i < $compositionsAttrs['pages'] + 1; $i++)
            @if ($i == 1)
                <div class="page-number active" data-offset="zero">1</div>
            @else
                <div class="page-number" data-offset={{ ($i - 1) * 9 }}>{{ $i }}</div>
            @endif
        @endfor
        {{--                <div id="page-next" class="page-number" data-offset="">&#11157;</div>--}}
    @endif
</div>
