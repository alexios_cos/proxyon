<div class="main-container" data-content="store">
    <div class="main-content">
        <div class="filters">
            <div class="current-filters">
                <div class="filters-options">
                    @if ($compositionsAttrs['tags'] || $compositionsAttrs['instruments'] || $compositionsAttrs['query'] || $compositionsAttrs['price'])
                        <div class="clear-all-filters">&#10006;</div>
                    @endif
                    @if ($compositionsAttrs['query'] || $compositionsAttrs['price'])
                        <div class="search-filters">
                            @if ($compositionsAttrs['query'])
                                <div class="request-filter">
                                    <div class="user-query" data-query="{{ $compositionsAttrs['query'] }}" data-query-type="{{ $compositionsAttrs['query-type'] }}"><span class="faded-legend">Search</span> {{ $compositionsAttrs['query-type'] }}: " {{ $compositionsAttrs['query'] }} "</div>
                                    <div class="filter-delete-sign">&#xd7;</div>
                                </div>
                            @endif
                            @if ($compositionsAttrs['price'])
                                <div class="prices-filter">
                                    <div>${{ $compositionsAttrs['price']['minPrice'] }} - ${{ $compositionsAttrs['price']['maxPrice'] }}</div>
                                    <div class="filter-delete-sign">&#xd7;</div>
                                </div>
                            @endif
                        </div>
                    @endif
                    @if ((($compositionsAttrs['query'] || $compositionsAttrs['price']) && $compositionsAttrs['tags']) || (($compositionsAttrs['query'] || $compositionsAttrs['price']) && $compositionsAttrs['instruments']))
                        <div class="filters-spacer"></div>
                    @endif
                    @if ($compositionsAttrs['tags'])
                        <div class="tags-filters">
                            @foreach ($compositionsAttrs['tags'] as $tag)
                                @if ($loop->iteration == 6)
                                    <div class="musical-filters-more">+ {{ $loop->remaining + 1 }} more</div>
                                    @break
                                @else
                                <div class="tag-filter" data-name="{{ $tag['kebab'] }}">
                                    <div>{{ $tag['space'] }}</div>
                                    <div class="filter-delete-sign">&#xd7;</div>
                                </div>
                                @endif
                            @endforeach
                        </div>
                    @endif
                    @if ($compositionsAttrs['tags'] && $compositionsAttrs['instruments'])
                        <div class="filters-spacer"></div>
                    @endif
                    @if ($compositionsAttrs['instruments'])
                        <div class="instruments-filters">
                            @foreach ($compositionsAttrs['instruments'] as $instrument)
                                @if ($loop->iteration == 5)
                                    <div class="musical-filters-more">+ {{ $loop->remaining + 1 }} more</div>
                                    @break
                                @else
                                <div class="instrument-filter" data-name="{{ $instrument['kebab'] }}">
                                    <div>{{ $instrument['space'] }}</div>
                                    <div class="filter-delete-sign">&#xd7;</div>
                                </div>
                                @endif
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="compositions-options">
                <div class="composition-search">
                    <span id="composition-search-button"><i class="fa fa-search"></i></span>
                    <input type="text" id="composition-search-input" placeholder="Search within current results...">
                    <label for="composition-search-input"></label>
                    <div class="composition-search-type composition-type">
                        <div class="dropdown-icon composition-type current" title="Composition" data-type="composition"><span>&#9836;</span></div>
                        <div class="composition-search-type-button"><span>&#8250;</span></div>
                    </div>
                </div>
                <div class="sorters">
                    <div id="first-sorter" class="sorter current" data-sort="name" data-sort-direction-next="desc" data-sort-direction-current="asc">
                        <span class="sort-name">Name</span>
                        <span class="sort-sign">&#8657;</span>
                    </div>
                    <div class="sorter basic" data-sort="price" data-sort-direction-next="asc">
                        <span class="sort-name">Price</span>
                        <span class="sort-sign">&#8652;</span>
                    </div>
                    <div class="sorter basic" data-sort="id" data-sort-direction-next="asc">
                        <span class="sort-name">Date</span>
                        <span class="sort-sign">&#8652;</span>
                    </div>
                    <div class="sorter basic" data-sort="marks" data-sort-direction-next="asc">
                        <span class="sort-name">Marks</span>
                        <span class="sort-sign">&#8652;</span>
                    </div>
                    <div id="last-sorter" class="sorter basic" data-sort="sales" data-sort-direction-next="asc">
                        <span class="sort-name">Sales</span>
                        <span class="sort-sign">&#8652;</span>
                    </div>
                </div>
                <div class="category-filters">
                    <div class="genre-filters">
                        @foreach($compositionsAttrs['genre-path'] as $genre)
                            @if ($loop->last)
                                <div class="genre-filter current-genre">{{ $genre }}</div>
                            @else
                                <div class="genre-filter back-genre">{{ $genre }}</div><span> / </span>
                            @endif
                        @endforeach
                    </div>
                    @if ($compositionsAttrs['amount'] > 1)
                        <div>{{ $compositionsAttrs['amount'] }} tracks in </div>
                    @else
                        <div>{{ $compositionsAttrs['amount'] }} track in </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="compositions-container">
            <div class="compositions">
                @foreach ($compositions as $composition)
                    <div class="composition" data-id="{{ $composition['composition']['id'] }}">
                        <audio src="{{ $composition['composition']['path'] }}" controls="controls"></audio>
                        <div class="composition-info">
                            <div class="composition-name composition-attr">{{ $composition['composition']['name'] }}</div>
                            @if (isset($composition['anthology']['name']))
                                <div class="composition-anthology composition-attr"><i>from </i>{{ $composition['anthology']['name'] }}</div>
                            @endif
                            <div class="composition-artist composition-attr"><i>by </i>{{ $composition['artist']['name'] }}</div>
                        </div>
                        <div class="composition-features">
                            <div class="composition-marks composition-feature">{{ $composition['composition']['marks'] }} marks</div>
                            <div class="composition-sales composition-feature">{{ $composition['composition']['sales'] }} sales</div>
                            <div class="composition-price composition-feature">{{ $composition['composition']['price'] }} $</div>
                        </div>
                        <div class="composition-actions">
                            <div class="composition-mark composition-action" title="Mark composition">&#9733;</div>
                            <div class="composition-wish composition-action" title="Add to wishlist">&#10084;</div>
                            <div class="composition-buy composition-action" title="Add to cart">&#36;</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="composition-pages">
            @if ($compositionsAttrs['pages'] > 1)
                @for ($i = 1; $i < $compositionsAttrs['pages'] + 1; $i++)
                    @if ($i == 1)
                        <div class="page-number active" data-offset="zero">1</div>
                    @else
                        <div class="page-number" data-offset={{ ($i - 1) * 9 }}>{{ $i }}</div>
                    @endif
                @endfor
{{--                <div id="page-next" class="page-number" data-offset="">&#10140;</div>--}}
            @endif
        </div>
    </div>
</div>
