<!DOCTYPE html>
<html lang="en">

    <head>
{{--        <base href="/var/www/proxyon">--}}
        <meta charset="UTF-8">
        <title>Welcome</title>
        <meta name="description" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="{{ asset('js/jquery/jquery-3.4.1.min.js') }}"></script>
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|El+Messiri:400,700|IM+Fell+Great+Primer|Quicksand:400,700&display=swap" rel="stylesheet">
    </head>

    <body>
        <div id="nav">
            <div id="nav-menu">
                <a href="library" class="nav-tab" data-ajax="true">read</a>
                <a href="store" class="nav-tab" data-ajax="true">shop</a>
                <div class="nav-tab">play</div>
{{--                <a href="collection" class="nav-tab" data-ajax="true">play</a>--}}
            </div>
            <header>
                <div>
                    <h1>Proxyon</h1>
                    <p>music library</p>
                </div>
            </header>
            <div id="nav-utilities">
                <div class="nav-tab">
                    <div class="nav-tab-content">
                        <span>cart</span>
                    </div>
                </div>
                <div class="nav-tab">
                    <div class="nav-tab-content">
                        <span>find</span>
                    </div>
                </div>
                <div class="nav-tab">
                    <div class="nav-tab-content">
                        <span>auth</span>
                    </div>
                </div>
            </div>
        </div>

        <div id="content" class="cf">
            <div id="sidebar-menu-container">
                <div id="sidebar-menu">
                    @yield('sidebar-menu')
                </div>
            </div>

            <div id="main-container">
                <div id="main">
                     @yield('main')
                </div>
            </div>

            <div id="sidebar-utilities-container">
                <div id="sidebar-utilities">
                    @yield('sidebar-utilities')
                </div>
            </div>
        </div>

        <script src="{{ asset('js/scripts.js') }}"></script>
    </body>

</html>
