<div class="sidebar-list" data-sidebar="library">
    <div class="sidebar-list-content invisible">
        @foreach ($genres as $genre)
            <div class="accordion-section">
                <div class="accordion-button off">
                    <div class="genre-name"><span>{{ $genre['name'] }}</span></div>
                    <div class="change-status-sign"><span>&#8250;</span></div>
                </div>
                <div class="sub-genre-list">
                    @foreach ($genre['sub-genres'] as $subGenre)
                        <div class="sub-genre-name" data-category="{{ $subGenre['id'] }}">{{ $subGenre['name'] }}</div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
</div>
