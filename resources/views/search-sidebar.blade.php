<div id="search-field">
    <form action="" method="">
        @csrf
        <div class="search-field-element" id="search-input-container">
            <label for="search-input"></label>
            <input type="text" name="" id="search-input" placeholder="Search...">
        </div>
        <div class="search-field-element" id="search-button-container">
            <span id="search-button"><i class="fa fa-search"></i></span>
        </div>
    </form>
</div>
<div id="search-category">
    <div class="search-category-head">
        <span>Category:</span>
    </div>
    <div class="search-category-options">
        <div class="search-category-option">
            <div class="search-category-option-check"></div>
            <span>Library</span>
        </div>
        <div class="search-category-option">
            <div class="search-category-option-check"></div>
            <span>Store</span>
        </div>
        <div class="search-category-option">
            <div class="search-category-option-check"></div>
            <span>Collection</span>
        </div>
    </div>
</div>
<div id="search-filters">
    <div class="search-category-head">
        <span>Filter:</span>
    </div>
    <div class="search-category-options">
        <div class="search-category-option">
            <div class="search-category-option-check"></div>
            <span>Composition</span>
        </div>
        <div class="search-category-option">
            <div class="search-category-option-check"></div>
            <span>Album</span>
        </div>
        <div class="search-category-option">
            <div class="search-category-option-check"></div>
            <span>Artist</span>
        </div>
    </div>
</div>
