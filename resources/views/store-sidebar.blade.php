<div class="sidebar-list" data-sidebar="store">
    <div class="sidebar-list-content invisible">
        <div class="accordion-section">
            <div class="accordion-button on">
                <div class="genre-name"><span>Category</span></div>
                <div class="change-status-sign"><span>&#8250;</span></div>
            </div>
            <div class="sub-genre-list visible">
                <div class="categories">
                    <div class="category-button current" data-category="{{ $categories['category']['data-category'] }}">
                        <div class="back-sign"><span>&#9668;</span></div>
                        <div class="category-name">{{ $categories['category']['name'] }}</div>
                        <div class="category-content-amount">{{ $categories['category']['content-amount'] }}</div>
                    </div>
                </div>
                <div class="sub-categories-container">
                    <div class="sub-categories">

                        @foreach($categories['subcategories'] as $subCategory)
                            <div class="sub-category-button" data-category="{{ $subCategory['data-category'] }}">
                                <div class="sub-category-name">{{ $subCategory['name'] }}</div>
                                <div class="sub-category-content-amount">{{ $subCategory['content-amount'] }}</div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-section">
            <div class="accordion-button on">
                <div class="genre-name"><span>Tags</span></div>
                <div class="change-status-sign"><span>&#8250;</span></div>
            </div>
            <div class="sub-genre-list visible">
                @foreach($tags as $tag)
                    <div class="checkbox">
                        <input class="checkbox-input" type="checkbox" id="{{ $tag['name']['kebab'] }}-checkbox" value="{{ $tag['name']['kebab'] }}" data-type="tag" onclick="return false">
                        <label class="checkbox-label" for="{{ $tag['name']['kebab'] }}-checkbox" onclick="return false">{{ $tag['name']['space'] }}</label>
                    </div>
                @endforeach
                <div class="checkbox">
                    <input class="checkbox-input" type="checkbox" id="untagged-checkbox" value="untagged" data-type="tag" onclick="return false">
                    <label class="checkbox-label" for="untagged-checkbox" onclick="return false">untagged</label>
                </div>
            </div>
        </div>
        <div class="accordion-section">
            <div class="accordion-button on">
                <div class="genre-name"><span>Instruments</span></div>
                <div class="change-status-sign"><span>&#8250;</span></div>
            </div>
            <div class="sub-genre-list visible">
                @foreach($instruments as $instrument)
                    <div class="checkbox">
                        <input class="checkbox-input" type="checkbox" id="{{ $instrument['name']['kebab'] }}-checkbox" value="{{ $instrument['name']['kebab'] }}" data-type="instrument" onclick="return false">
                        <label class="checkbox-label" for="{{ $instrument['name']['kebab'] }}-checkbox" onclick="return false">{{ $instrument['name']['space'] }}</label>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="accordion-section">
            <div class="accordion-button on">
                <div class="genre-name"><span>Price</span></div>
                <div class="change-status-sign"><span>&#8250;</span></div>
            </div>
            <div class="sub-genre-list visible">
                <div class="price-filter">
                    <div class="currency-sign-min">
                        <span>$</span>
                    </div>
                    <input type="text" id="min-price" class="price-input" placeholder="{{ $minPrice }}">
                    <label for="min-price"></label>
                    <div class="price-delimiter">
                        <span>&#8211;</span>
                    </div>
                    <div class="currency-sign-max">
                        <span>$</span>
                    </div>
                    <input type="text" id="max-price" class="price-input" placeholder="{{ $maxPrice }}">
                    <label for="max-price"></label>
                    <div class="price-button">
                        <span>&#10097;</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
