<div class="sub-categories">
    @foreach($subCategories as $subCategory)
        <div class="sub-category-button" data-category="{{ $subCategory['data-category'] }}">
            <div class="sub-category-name">{{ $subCategory['name'] }}</div>
            <div class="sub-category-content-amount">{{ $subCategory['content-amount'] }}</div>
        </div>
    @endforeach
</div>
