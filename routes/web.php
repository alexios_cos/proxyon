<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::view('/', 'proxy-welcome');

Route::redirect('/store', '/');

Route::redirect('/library', '/');

Route::redirect('/collection', '/');

Route::get('welcomeAjaxRequest', 'WelcomeController@sendView');

Route::get('sidebarMenuAjaxRequest', 'SidebarMenuController@sendView');

Route::post('sidebarMenuAjaxDelivery', 'SidebarMenuController@storeView');

Route::get('sidebarMenuAjaxDirective', 'SidebarMenuController@eraseStoredViews');

Route::get('mainAjaxRequest', 'MainController@sendView');

Route::get('storeSidebarAjaxRequest', 'StoreSidebarController@sendView');

Route::get('storeAjaxRequest', 'StoreController@sendViewByRequest');

Route::post('storeAjaxRequest', 'StoreController@sendViewByRequest');

Route::get('libraryAjaxRequest', 'LibraryController@sendViewByRequest');

Route::get('storeMainAjaxRequest', 'StoreController@sendViewByRequest');

Route::get('searchTypeButtonAjaxRequest', 'SearchTypeButtonController@sendView');

Route::get('/test-ground', 'TestGroundController@test');
